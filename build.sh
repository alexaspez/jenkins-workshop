#!/usr/bin/env bash 
# set -x
#########################################
#created by Silent-Mobius
#purpose: build script for jenkins class
#verion: 0.5.21
#########################################

. /etc/os-release

PROJECT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BUILD_DIR_ARRAY=($(ls |grep -vE '99_*|README.md|TODO.md|build.sh|LICENSE'))
BUILD_FILE='build.md'
SEPERATOR='-------------------------------------------'


main(){
    if [[ ${#} -le 0 ]];then
        _help
    fi

        get_installer
        get_builder

    
    while getopts "bch" opt
    do
        case $opt in
            b)  seek_all_md_files
                convert_data
                ;;
            c) clean_up
                ;;
            h) _help
                ;;
            *) _help
                ;;
        esac
    done

}

function _help() {
    printf "\n%s \n%s \n%s\n " "[?] Incorrect use" \
                               "[?] Please use $0 \"-b\" for build and \"-c\" for clean up" \
                               "[?] example: $0 -c"
}

function clean_up() {
    printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                               '[+] Cleaning Up The Previous Builds' \
                               "$SEPERATOR"
    if [ -e "$PROJECT/$BUILD_FILE" ];then
      rm -rf "$PROJECT/${BUILD_FILE:?}"
    fi

    if [ -e $PROJECT/presentation.html ];then
        rm -rf $PROJECT/presentation.html
    fi
    
    if [ -e $PROJECT/presentation.pdf ];then
        rm -rf $PROJECT/presentation.pdf
    fi
    find . -name presentation.html -exec rm {} \;
    printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                               '[+] Cleanup Ended Successfully   ' \
                               "$SEPERATOR"
}

function get_installer(){
    if [[ ${ID,,} == 'debian' ]] || [[ ${ID,,} == 'debian' ]] || [[ ${ID,,} == 'linuxmint' ]];then
        export INSTALLER=apt-get
    elif [[ ${ID,,} == 'redhat' ]] || [[ ${ID,,} == 'fedora' ]] || [[ ${ID,,} == 'rocky' ]];then
        export INSTALLER=dnf
    else  
        clear
        printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                                   '[!] OS Not Supported [!]   ' \
                                   "$SEPERATOR"
        sleep 2
        printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                                   '[+] Please Contact Instructor   ' \
                                   "$SEPERATOR"
        sleep 2
        exit 1
    fi
}

function get_builder(){
    if [[ -e /usr/bin/darkslide ]];then
        BUILDER='/usr/bin/darkslide'
    elif [[ -e /usr/bin/landslide ]];then
        BUILDER='/usr/bin/landslide'
    else
        printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                                   '[+] Dependency Missing: Trying To Fix   ' \
                                   "$SEPERATOR"
        dep_fix=$(sudo ${INSTALLER} install -y python3-landslide &> /dev/null;echo $?)
        if [[ $dep_fix == 0 ]];then   
                printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                                           '[+] Install Ended Successfully   ' \
                                           "$SEPERATOR"
                    if [[ -e /usr/bin/darkslide ]];then
                        BUILDER='/usr/bin/darkslide'
                    elif [[ -e /usr/bin/landslide ]];then
                        BUILDER='/usr/bin/landslide'
                    fi
        else
            printf "\n%s \n%s \n%s\n %s\n" "$SEPERATOR" \
                                                     '[!] Install Failed [!] ' \
                                                     '[+] Please Contact Instructor   ' \
                                                     "$SEPERATOR"
            exit 1
        fi
    fi

}

function seek_all_md_files() {
    clean_up
    printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                               '[+] Building Presentation' \
                               "$SEPERATOR"
    find "${BUILD_DIR_ARRAY[@]}" -name '*.md'|sort|xargs cat > build.md 2> /dev/null

    printf "\n%s \n%s \n%s\n " "$SEPERATOR" \
                               '[+] Generate ended successfully  ' \
                               "$SEPERATOR"
}

function convert_data() {
    printf "\n%s \n%s \n%s\n" "$SEPERATOR" \
                               '[+] Converting Data     ' \
                               "$SEPERATOR"
        if [[ $ID == 'ubuntu' ]] || [[ $ID == 'linuxmint' ]];then # there is a bug on ubuntu and linuxmint ubuntu based distro - not using regular darkslide
            ${BUILDER} -v  -t "${PROJECT}/99_misc/.theme/" -x fenced_code,codehilite,extra,toc,smarty,sane_lists,meta,tables build.md
        else
            ${BUILDER} -v  -t "${PROJECT}/99_misc/.theme/" -x fenced_code,codehilite,extra,toc,smarty,sane_lists,meta,md_in_html,tables build.md
        fi
}



#######
# Main
#######
main "$@"